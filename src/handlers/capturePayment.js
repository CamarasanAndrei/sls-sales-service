import createError from 'http-errors';
import AWS from 'aws-sdk';
import commonMiddleware from '../lib/commonMiddleware';
import { captureCharge } from '../lib/stripe';
import { emit } from '../lib/eventBus';

const dynamodb = new AWS.DynamoDB.DocumentClient();

async function capturePayment(event, context) {
  const { charge, id } = event.detail;
  const updatedCharge = await captureCharge(charge.id);
  const params = {
    TableName: process.env.SALES_TABLE_NAME,
    Key: { id },
    UpdateExpression: 'set charge = :charge',
    ExpressionAttributeValues: {
      ':charge': updatedCharge
    },
    ReturnValues: 'ALL_NEW'
  };

  try {
    const result = await dynamodb.update(params).promise();
    const updatedSale = result.Attributes;
    const transactionParams = {
      charge: updatedCharge,
      orderId: updatedSale.order.entityId
    }
    await emit(transactionParams, 'TransactionOrder', 'local.aws');
    await emit({ orderId: updatedSale.order.entityId }, 'InvoiceOrder', 'local.aws');
  } catch (error) {
    console.log(error);
    throw new createError.InternalServerError(error);
  }
}

export const handler = commonMiddleware(capturePayment);