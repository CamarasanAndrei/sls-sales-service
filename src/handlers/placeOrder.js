import AWS from 'aws-sdk';
import commonMiddleware from '../lib/commonMiddleware';
import createError from 'http-errors';
import { emit } from '../lib/eventBus';

const dynamodb = new AWS.DynamoDB.DocumentClient();

async function placeOrder(event, context) {
  const { cart } = event.body;
  const { ccDetails } = cart;
  const { cartId } = event.pathParameters;
  let sale;
  
  try {
    delete cart.ccDetails;
    sale = {
      ...cart,
      status: 'OPEN',
      placedAt: new Date().toISOString(),
    };
    await dynamodb.put({
      TableName: process.env.SALES_TABLE_NAME,
      Item: sale
    }).promise();
    if (sale.billing.method.method === 'ccc') {
      const authParams = {
        ccDetails,
        totals: sale.totals,
        id: sale.id,
        cartId,
        billing: cart.billing
      };
      await emit(authParams, 'AuthorizeSale', 'local.aws');
    }
  } catch (error) {
    console.log(error);
    throw new createError.InternalServerError(error);
  }
  
  return {
    statusCode: 200,
    body: JSON.stringify(sale)
  };
}

export const handler = commonMiddleware(placeOrder);


